import control
import control.matlab as mat
from yaml import full_load
import numpy as np

def parseZPKModel(zpk):
    for key in ['z', 'p']:
        if key not in zpk:
            zpk[key] = []
        elif not isinstance(zpk[key], list):
            zpk[key] = [zpk[key]]
    z = 2*np.pi*np.array(zpk['z'], dtype=np.complex128)
    p = 2*np.pi*np.array(zpk['p'], dtype=np.complex128)
    zpk['ABCD'] = mat.zpk2ss(-z, -p, zpk['k'])
    zpk['SS'] = control.ss(*zpk['ABCD'])
    if 'delay' in zpk:
        num, den = control.pade(zpk['delay'], 4)
        zpk['DelayTF'] = control.tf(num, den)
        zpk['SS'] = control.series(zpk['SS'], zpk['DelayTF'])
    return zpk

def series(alsCtrl, subSys):
    out = alsCtrl[subSys[0]]['SS']
    for ii in range(1, len(subSys)):
        out = control.series(out, alsCtrl[subSys[ii]]['SS'])
    return out

def sys2TF(ss, freq):
    mag, ph, _ = control.freqresp(ss, 2 * np.pi * freq)
    return mag.flatten() * np.exp(1j*ph.flatten())

def parseZPKfromYaml(filename):
    with open(filename, 'r') as f:
        ctrlSys = full_load(f)
        for k, v in ctrlSys.items():
            for k1, v1 in v.items():
                if isinstance(v1, str):
                    try:
                        ctrlSys[k][k1] = eval(v1)
                    except BaseException:
                        ctrlSys[k][k1] = v1
                elif isinstance(v1, list):
                    for k2, v2 in enumerate(v1):
                        if isinstance(v2, str):
                            ctrlSys[k][k1][k2] = eval(v2)
            ctrlSys[k] = parseZPKModel(v)
    return ctrlSys
