"""
Hang Yu
hangyu@caltech.edu
Aug, 2021
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import scipy.interpolate as interp
import scipy.integrate as integ
import scipy.signal as sig
import h5py as h5
import os.path
import matplotlib.ticker as ticker


########################################
###         key functions            ###
########################################

def unpack_par_dict(par_dict):
    """
    unpack the par_dict into a numpy array that can be easily looped over
    """
    par = np.hstack([
        par_dict['zs_real'],
        par_dict['ps_real'],
        par_dict['zs_f'],
        par_dict['zs_Q'],
        par_dict['ps_f'],
        par_dict['ps_Q'],
        par_dict['ks']
    ])
    n_zr = len(par_dict['zs_real'])
    n_pr = len(par_dict['ps_real'])
    n_zc = len(par_dict['zs_f'])
    n_pc = len(par_dict['ps_f'])
    return par, n_zr, n_pr, n_zc, n_pc


def pack_par_to_dict(par, n_zr, n_pr, n_zc, n_pc):
    """
    inverting unpack_par_dict
    """
    zs_real = par[:n_zr]
    ps_real = par[n_zr:n_zr+n_pr]
    zs_f = par[n_zr+n_pr:n_zr+n_pr+n_zc]
    zs_Q = par[n_zr+n_pr+n_zc:n_zr+n_pr+2*n_zc]
    ps_f = par[n_zr+n_pr+2*n_zc:n_zr+n_pr+2*n_zc+n_pc]
    ps_Q = par[n_zr+n_pr+2*n_zc+n_pc:n_zr+n_pr+2*n_zc+2*n_pc]
    ks = par[-1]
    
    par_dict = {'zs_real':zs_real,
                'ps_real':ps_real,
                'zs_f':zs_f,
                'zs_Q':zs_Q,
                'ps_f':ps_f,
                'ps_Q':ps_Q,
                'ks':ks}
    return par_dict


def sort_par_dict(par_dict):
    zs_real = par_dict['zs_real']
    ps_real = par_dict['ps_real']
    zs_f = par_dict['zs_f']
    zs_Q = par_dict['zs_Q']
    ps_f = par_dict['ps_f']
    ps_Q = par_dict['ps_Q']
    ks = par_dict['ks']
    
    if len(zs_real)>0:
        zs_real = np.sort(zs_real)
        
    if len(ps_real)>0:
        ps_real = np.sort(ps_real)
        
    if len(zs_f)>0:
        idx_zs_f = np.argsort(zs_f)
        zs_f = zs_f[idx_zs_f]
        zs_Q = zs_Q[idx_zs_f]
        
    if len(ps_f)>0:
        idx_ps_f = np.argsort(ps_f)
        ps_f = ps_f[idx_ps_f]
        ps_Q = ps_Q[idx_ps_f]
        
    
    par_dict = {'zs_real':zs_real,
                'ps_real':ps_real,
                'zs_f':zs_f,
                'zs_Q':zs_Q,
                'ps_f':ps_f,
                'ps_Q':ps_Q,
                'ks':ks}
    
    return par_dict



def par_to_TF_vect(freq, par,
                   n_zr, n_pr, n_zc, n_pc):
    """
    compute the TF given by par [obtained from unpack_par_dict()]
    at a given frequncy grid freq
    """
    zs_real = par[:n_zr]
    ps_real = par[n_zr:n_zr+n_pr]
    zs_f = par[n_zr+n_pr:n_zr+n_pr+n_zc]
    zs_Q = par[n_zr+n_pr+n_zc:n_zr+n_pr+2*n_zc]
    ps_f = par[n_zr+n_pr+2*n_zc:n_zr+n_pr+2*n_zc+n_pc]
    ps_Q = par[n_zr+n_pr+2*n_zc+n_pc:n_zr+n_pr+2*n_zc+2*n_pc]
    ks = par[-1]

    zs = zs_real.copy()
    for i in range(n_zc):
        zs_r, zs_i = get_res_g_pole_pair(zs_f[i], zs_Q[i])
        zs = np.hstack([zs, zs_r+1j*zs_i, zs_r-1j*zs_i])

    ps = ps_real.copy()
    for i in range(n_pc):
        ps_r, ps_i = get_res_g_pole_pair(ps_f[i], ps_Q[i])
        ps = np.hstack([ps, ps_r+1j*ps_i, ps_r-1j*ps_i])

    __, GG = sig.freqs_zpk(zs, ps, ks, worN=2.*np.pi*freq)
    return GG

def par_to_sos(freq, par,
               n_zr, n_pr, n_zc, n_pc, 
               fs):
    """
    get filters in sos format based on par
    """
    zs_real = par[:n_zr]
    ps_real = par[n_zr:n_zr+n_pr]
    zs_f = par[n_zr+n_pr:n_zr+n_pr+n_zc]
    zs_Q = par[n_zr+n_pr+n_zc:n_zr+n_pr+2*n_zc]
    ps_f = par[n_zr+n_pr+2*n_zc:n_zr+n_pr+2*n_zc+n_pc]
    ps_Q = par[n_zr+n_pr+2*n_zc+n_pc:n_zr+n_pr+2*n_zc+2*n_pc]
    ks = par[-1]

    zs = zs_real.copy()
    for i in range(n_zc):
        zs_r, zs_i = get_res_g_pole_pair(zs_f[i], zs_Q[i])
        zs = np.hstack([zs, zs_r+1j*zs_i, zs_r-1j*zs_i])

    ps = ps_real.copy()
    for i in range(n_pc):
        ps_r, ps_i = get_res_g_pole_pair(ps_f[i], ps_Q[i])
        ps = np.hstack([ps, ps_r+1j*ps_i, ps_r-1j*ps_i])

    zz, pz, kz = sig.bilinear_zpk(zs, ps, ks, fs)
    sos = sig.zpk2sos(zz, pz, kz)
    return sos


def get_Fisher_from_psd(freq, par_dict,
                        Pxx, Pyy,
                        T_tot=None, n_avg=1,
                        G_known=1., 
                        dpar_dict=None, logflag_dict=None,
                        return_gamma_vs_freq=False):
    """
    Suppose we do a TF measurement of model G: 
             -----    ----------
        x -> | G | -> | G_known | -> y
             -----    ----------     
            where G is the model whose parameters we want to measure, 
            and G_known is the part in the TF which we know exactly (e.g., digital filters)
    We compute here the Fisher matrix of G's parameters.  

    We use expression given in 
    https://dcc.ligo.org/LIGO-G2101503, p19.

    This assumes we already have some prior knowledge of G, 
    and our estimation of G can be parameterized by par_dict
    [whose format should follow unpack_par_dict()].

    Pxx is the PSD of the excitation x
    Pyy is the PSD of the readout when there is NO excitation (ie, quiet-time PSD of the readout)
    Both are evaluated at the freq grid 
    
    G_known should be a real constant or a complex vector also evaluated at the freq grid.

    T_tot is the total time in [s] of the measurement.

    Alternatively, one can specify the number of averages n_avg = T_tot/T_perseg (no overlap)
    In this case, freq is assume uniformed spaced and df = 1/T_perseg
    Note, n_avg will be bypassed if T_tot is given.

    One can choose to manually set stepsize for each par when computing the Fisher numerically.
    This is done via the dpar_dict.
    It should be filled following the same order as par_dict.
    If dpar_dict=None, default stepsize of 1e-8 will be used on all the par.

    One can also choose to compute the absolute error or the fractional one using
        logflag_dict 
    (following the structure of par_dict; a par with flag>0 will return fractional error).
    If logflag_dict=None, it will return fractional error for ks=par[-1] and absolute error for the rest.
    """

    par, n_zr, n_pr, n_zc, n_pc = unpack_par_dict(par_dict)
    n_par = len(par)
    n_bin = len(freq)

    if T_tot is None:
        df = freq[1]-freq[0]
        T_tot = (1./df) * n_avg

    if dpar_dict is None:
        dpar = np.ones(n_par) * 1.e-8
    else:
        dpar, __, __, __, __ = unpack_par_dict(dpar_dict)

    if logflag_dict is None:
        logflag = -np.ones(n_par, dtype=np.int)
        logflag[-1] = 1
    else:
        logflag, __, __, __, __ = unpack_par_dict(dpar_dict)

    dG_dpar = np.zeros([n_par, n_bin], dtype=np.complex)

    for i in range(n_par):
        par_u = par.copy()
        par_u[i] += dpar[i]
        GG_u = par_to_TF_vect(freq, par_u,
                              n_zr, n_pr, n_zc, n_pc)

        par_l = par.copy()
        par_l[i] -= dpar[i]
        GG_l = par_to_TF_vect(freq, par_l,
                              n_zr, n_pr, n_zc, n_pc)

        dG_dpar[i, :] = (GG_u-GG_l)/(2.*dpar[i])

        if logflag[i] > 0:
            dG_dpar[i, :] *= par[i]

    gamma = np.zeros([n_par, n_par])
    for i in range(n_par):
        for j in range(i, n_par, 1):
            gamma[i, j] = 2.*integ.trapz(
                np.real(np.conj(dG_dpar[i, :])*dG_dpar[j, :])* G_known**2. * Pxx/Pyy,
                freq)

    for i in range(n_par):
        for j in range(i):
            gamma[i, j] = gamma[j, i]

    gamma *= T_tot

    if return_gamma_vs_freq:
        gamma_vs_freq = np.zeros([n_par, n_par, n_bin])

        for i in range(n_par):
            for j in range(i, n_par, 1):
                gamma_vs_freq[i, j, :] =\
                    2.*np.real(np.conj(dG_dpar[i, :])*dG_dpar[j, :])* G_known**2. * Pxx/Pyy

        for i in range(n_par):
            for j in range(i):
                gamma_vs_freq[i, j, :] = gamma_vs_freq[j, i, :]

        gamma_vs_freq *= T_tot
        return gamma, gamma_vs_freq

    else:
        return gamma
    
    
def get_Fisher_from_coh(freq, par_dict,
                        coh,
                        T_tot=None, n_avg=1, 
                        dpar_dict=None, logflag_dict=None,
                        return_gamma_vs_freq=False):
    """
    Suppose we do a TF measurement of model G: 
             -----    ----------
        x -> | G | -> | G_known | -> y
             -----    ----------     
            where G is the model whose parameters we want to measure, 
            and G_known is the part in the TF which we know exactly (e.g., digital filters)
    We compute here the Fisher matrix of G's parameters.  

    We use expression given in 
    https://dcc.ligo.org/LIGO-G2101503, p19.

    Instead of using the PSDs of the excitation and the exc-free readout, 
    we instead use the coherence of the measurement. 
    
    Here coh should be a vector containing the coherence 
    evaluated the freq grid.
    
    Other inputs are the same as in get_Fisher_from_psd().
    """

    par, n_zr, n_pr, n_zc, n_pc = unpack_par_dict(par_dict)
    n_par = len(par)
    n_bin = len(freq)
    GG = par_to_TF_vect(freq, par,
                        n_zr, n_pr, n_zc, n_pc)

    if T_tot is None:
        df = freq[1]-freq[0]
        T_tot = (1./df) * n_avg

    if dpar_dict is None:
        dpar = np.ones(n_par) * 1.e-8
    else:
        dpar, __, __, __, __ = unpack_par_dict(dpar_dict)

    if logflag_dict is None:
        logflag = -np.ones(n_par, dtype=np.int)
        logflag[-1] = 1
    else:
        logflag, __, __, __, __ = unpack_par_dict(dpar_dict)

    dG_dpar = np.zeros([n_par, n_bin], dtype=np.complex)

    for i in range(n_par):
        par_u = par.copy()
        par_u[i] += dpar[i]
        GG_u = par_to_TF_vect(freq, par_u,
                              n_zr, n_pr, n_zc, n_pc)

        par_l = par.copy()
        par_l[i] -= dpar[i]
        GG_l = par_to_TF_vect(freq, par_l,
                              n_zr, n_pr, n_zc, n_pc)

        dG_dpar[i, :] = (GG_u-GG_l)/(2.*dpar[i])

        if logflag[i] > 0:
            dG_dpar[i, :] *= par[i]


    gamma = np.zeros([n_par, n_par])
    for i in range(n_par):
        for j in range(i, n_par, 1):
            gamma[i, j] = 2.*integ.trapz(
                np.real(np.conj(dG_dpar[i, :])*dG_dpar[j, :]) / np.abs(GG)**2. \
                    * coh/(1-coh),
                freq)

    for i in range(n_par):
        for j in range(i):
            gamma[i, j] = gamma[j, i]

    gamma *= T_tot

    if return_gamma_vs_freq:
        gamma_vs_freq = np.zeros([n_par, n_par, n_bin])

        for i in range(n_par):
            for j in range(i, n_par, 1):
                gamma_vs_freq[i, j, :] =\
                    2.*np.real(np.conj(dG_dpar[i, :])*dG_dpar[j, :]) / np.abs(GG)**2.\
                        * coh/(1-coh)

        for i in range(n_par):
            for j in range(i):
                gamma_vs_freq[i, j, :] = gamma_vs_freq[j, i, :]

        gamma_vs_freq *= T_tot
        return gamma, gamma_vs_freq

    else:
        return gamma


def get_dispersion(freq, par_dict,
                   Pxx, Pyy,
                   T_tot=None, n_avg=1,
                   G_known = 1., 
                   dpar_dict=None, logflag_dict=None,
                   return_gamma=False):
    """
    Compute the dispersion function following Pintelon & Schoukens
    Sec. 5.4.2

    Note one needs to specify T_tot only if one cares about the proper value of gamma.
    If one just want the optimized PSD, T_tot can be simply set to 1. 
    """
    n_bin = len(freq)
    gamma, gamma_vs_freq = \
        get_Fisher_from_psd(freq, par_dict,
                            Pxx, Pyy,
                            T_tot=T_tot, n_avg=n_avg,
                            G_known=G_known,
                            dpar_dict=dpar_dict, logflag_dict=logflag_dict,
                            return_gamma_vs_freq=True)
    sigma = np.linalg.inv(gamma)
    nu = np.zeros(n_bin)

    Pxx_tot = np.sum(Pxx)

    for i in range(n_bin):
        gamma_loc = gamma_vs_freq[:, :, i] * Pxx_tot / Pxx[i]
        nu[i] = np.trace(sigma @ gamma_loc)

    if return_gamma:
        return nu, gamma
    else:
        return nu


def get_opt_exc_Pxx(freq, par_dict,
                    Pyy, Px_tot,
                    Pxx = None,
                    n_iter=3,
                    T_tot=None, n_avg=1,
                    G_known=1., 
                    dpar_dict=None, logflag_dict=None,
                    rec_progress=False):
    """
    Excitation optimization algorithm following Pintelon & Schoukens
    Sec. 5.4.2.2

    Use n_iter to control the numer of iterations to perform. 
    Typically n_iter=2 or 3 would be recommended, 
    otherwise the result would depend too heavily on the prior knowledge. 

    If just want the final PSD of the excitation, 
    set rec_progress=False.

    If want to examine how the fisher matrix & dispersion function changes,
    set rec_progress=True.
    """

    par, __, __, __, __ = unpack_par_dict(par_dict)

    n_par = len(par)
    n_bin = len(freq)
    Pxx_rec = np.zeros([n_iter, n_bin])
    nu_rec = np.zeros([n_iter, n_bin])
    gamma_rec = np.zeros([n_iter, n_par, n_par])

    # init
    # one can also start with a none-white initial excitation by passing an explicit Pxx. 
    if Pxx is None:
        Pxx = np.ones(n_bin)  
        
    Px_tot_temp = integ.trapz(Pxx, freq)
    Pxx *= Px_tot/Px_tot_temp

    cnt = 0
    while cnt < n_iter:
        nu, gamma = get_dispersion(freq, par_dict,
                                   Pxx, Pyy,
                                   T_tot=T_tot, n_avg=n_avg,
                                   G_known=G_known, 
                                   dpar_dict=dpar_dict, logflag_dict=logflag_dict,
                                   return_gamma=True)

        Pxx = Pxx*nu
        Px_tot_temp = integ.trapz(Pxx, freq)
        Pxx *= Px_tot/Px_tot_temp

        Pxx_rec[cnt, :] = Pxx
        nu_rec[cnt, :] = nu
        gamma_rec[cnt, :, :] = gamma

        cnt += 1

    if rec_progress:
        return Pxx_rec, nu_rec, gamma_rec
    else:
        return Pxx
    
def update_par_dict_from_data(freq, G_data, G_err, par_dict0, 
                              G_known=1.):
    """
    update the model based on new measurements. 
    
    For model give by:
             -----    ----------
        x -> | G | -> | G_known | -> y
             -----    ----------     
    We have:
    G_err/ |G| = 1 / SNR
    """
    par0, n_zr, n_pr, n_zc, n_pc = unpack_par_dict(par_dict0)
    n_par = len(par0)    
    df = freq[1] - freq[0]
    
    def get_resi(par_):
        GG_ = par_to_TF_vect(freq, par_,
                   n_zr, n_pr, n_zc, n_pc)
        # residual at each freq bin
        resi = np.abs(GG_*G_known - G_data)**2./G_err**2.
        # integrate over bins
        resi = integ.trapz(resi, freq)/df
#         print(par_, '%e'%resi)
        return resi
    
#     par=opt.minimize(get_resi, par0, method='Nelder-Mead',  
#                      options={'disp': False, 
#                               'maxiter':int(n_par * 1000), 
#                               'fatol':1e-8, 
#                               'adaptive': True})
    par=opt.minimize(get_resi, par0,  
                     options={'disp': False, 
                              'maxiter':int(n_par * 1000)})
    par=par.x
    par_dict = pack_par_to_dict(par, n_zr, n_pr, n_zc, n_pc)
    return par_dict




def update_par_dict_from_data_backup(freq, G_data, G_err, par_dict0, 
                              dpar_dict=None):
    """
    update the model based on new measurements. 
    
    For model give by:
            -----
        x ->| G |-> y
            -----        
    We have:
    G_err/ |G| = 1 / SNR
    """
    par0, n_zr, n_pr, n_zc, n_pc = unpack_par_dict(par_dict0)
    n_par = len(par0)
    
    if dpar_dict is None:
        dpar = np.ones(n_par) * 1.e-8
    else:
        dpar, __, __, __, __ = unpack_par_dict(dpar_dict)
    
    df = freq[1] - freq[0]
    def get_jac(par_):
        jac = np.zeros(len(par_))
        GG_ = par_to_TF_vect(freq, par_,
                   n_zr, n_pr, n_zc, n_pc)
        
        for i in range(n_par):
            par_u = par_.copy()
            par_u[i] += dpar[i]
            GG_u = par_to_TF_vect(freq, par_u,
                              n_zr, n_pr, n_zc, n_pc)
            par_l = par_.copy()
            par_l[i] -= dpar[i]
            GG_l = par_to_TF_vect(freq, par_l,
                              n_zr, n_pr, n_zc, n_pc)

            dG_dpar = (GG_u-GG_l)/(2.*dpar[i])
            jac_ = 2.*np.real(dG_dpar.conj() * (GG_ - G_data)) / G_err**2.
            jac[i] = integ.trapz(jac_, freq)/df
        return jac
    
    def get_resi(par_):
        GG_ = par_to_TF_vect(freq, par_,
                   n_zr, n_pr, n_zc, n_pc)
        # residual at each freq bin
        resi = np.abs(GG_ - G_data)**2./G_err**2.
        # integrate over bins
        resi = integ.trapz(resi, freq)/df
#         print(par_, '%e'%resi)
        return resi
    
    par=opt.minimize(get_resi, par0, jac=get_jac,
                     options={'disp': False})
    par=par.x
    par_dict = pack_par_to_dict(par, n_zr, n_pr, n_zc, n_pc)
    par_dict = sort_par_dict(par_dict)
    return par_dict



########################################
###        MCMC functions            ###
########################################


def log_likelihood(par, freq, GG, coh, n_avg, 
                   n_zr, n_pr, n_zc, n_pc):
    GG_model = sysID.par_to_TF_vect(freq, par,
                                  n_zr, n_pr, n_zc, n_pc)
    SNRsq = n_avg * coh / (1.-coh)
    log_prob = -0.5 * np.sum( SNRsq * np.abs(GG_model - GG)**2./np.abs(GG)**2.)
    return log_prob

def log_probability(par, freq, GG, coh, n_avg, 
                    n_zr, n_pr, n_zc, n_pc, 
                    log_prior_func):
    lp = log_prior_func(par, 
                   n_zr, n_pr, n_zc, n_pc)
    if not np.isfinite(lp):
        return -np.inf
    log_prob = lp + log_likelihood(par, freq, GG, coh, n_avg, 
                                   n_zr, n_pr, n_zc, n_pc)
    return log_prob



########################################
###     auxiliary functions          ###
########################################

def time_series_from_asd_vect(
        sec,
        fs,
        freq_in, asd_in,
        seed      = None, ):
    """
    Generate noise using tabulated ASD vector, asd_in, evaluated at frequency given by freq_in
    Parameters
    ----------
    sec : integer
        Length of time series in seconds.
    fs : integer
        Sampling frequency of time series in Hz. Defaults to 2048 Hz
    asd_file: path to the ASD file. The first column should be freq in [Hz] and the second one displacement in [m/rtHz]
    seed : int or np.random.default_rng(), optional
        Defaults to ``None``.
    Returns
    -------
    data : ndarray, shape (sec*fs,)
        Time series signal imitating LIGO noise.
    """

    if isinstance(seed, np.random._generator.Generator):
        rng = seed
    else:
        rng = np.random.default_rng(seed)

    # generate the data at 2*sec, take the middle part to avoid boundaries
    Ndoub = int(2 * sec * fs)
    N = int(sec*fs)
    Nfft = _nextpow2(Ndoub)

    data = rng.standard_normal(Nfft) 

    freq = np.fft.rfftfreq(Nfft, d=1 / fs)
    data_fft = np.fft.rfft(data * sig.tukey(Nfft, 0.2)) 
    df = freq[1]-freq[0]
    
    idx=np.where(freq_in>0)
    asd_vs_freq_func = interp.interp1d(freq_in[idx], np.log(asd_in[idx]), \
                                 bounds_error=False, fill_value=-np.inf)
    desired_asd = np.exp(asd_vs_freq_func(freq))

    # original asd ~ sqrt(2/fs)
    data_fft *= desired_asd * np.sqrt(fs/2.)
    
    data = np.fft.irfft(data_fft)
    # take the middle part to avoid boundary effects
    data = data[int(N/2):int(N/2)+N]
    return data

def _nextpow2(n):
    p = int(np.ceil(np.log2(n)))
    return 2**p

# convert from (f0, Q) to (a+1j*b, a-1j*b)
def get_res_g_pole_pair(f0, Q):
    """
    a, b are the real and imag parts of the complex pole
    """
    w0 = 2*np.pi * f0
    a = -w0/2/Q
    b = np.sqrt(w0**2 - a**2)

#     a=np.round(a, 6)
#     b=np.round(b, 6)

    return a, b


def get_res_f0_Q(a, b):
    """
    a, b are the real and imag parts of the complex pole
    """
    w0 = np.sqrt(a**2 + b**2)
    f0 = w0/2/np.pi
    Q = -w0/2/a
    return f0, Q


# codes for plotting the error ellipses
def get_prob_contour(x0, w, v):
    """
    x0 is the true value (center of the ellipse)
    w is the eigenvalue  (semi-major/minor axes)
    v is the matrix formed by eigenvectors (for rotation)
    """
    nPt = 1000
    vT = v.transpose()
    y0 = np.dot(vT, x0)
    y1 = np.linspace(y0[0] - np.sqrt(w[0]), y0[0] + np.sqrt(w[0]), nPt)
    y2 = np.zeros([2, nPt], dtype=np.complex)
    y2[0, :] = y0[1]+np.sqrt(w[1])*np.sqrt(1.+0j-(y1-y0[0])**2./(w[0]))
    y2[1, :] = y0[1]-np.sqrt(w[1])*np.sqrt(1.+0j-(y1-y0[0])**2./(w[0]))

    x1 = np.zeros([2, nPt])
    x2 = np.zeros([2, nPt])
    for i in range(nPt):
        y_temp = np.array([y1[i], np.real(y2[0, i])])
        x_temp = np.dot(v, y_temp)
        x1[0, i] = x_temp[0]
        x2[0, i] = x_temp[1]

        y_temp = np.array([y1[i], np.real(y2[1, i])])
        x_temp = np.dot(v, y_temp)
        x1[1, i] = x_temp[0]
        x2[1, i] = x_temp[1]
    return x1, x2


def plot_prob_contour(sigma, theta, idx,
                      ax=None, label='', **ax_kwargs):
    """
    sigma is the covariance matrix
    theta is the list of true values
    idx is the indices for the two components whose covariance we want to check
    ax is an axis object onto which we plot the contour
    """
    err_mtrx = np.zeros([2, 2])
    err_mtrx[0, 0] = sigma[idx[0], idx[0]]
    err_mtrx[0, 1] = sigma[idx[0], idx[1]]
    err_mtrx[1, 0] = sigma[idx[1], idx[0]]
    err_mtrx[1, 1] = sigma[idx[1], idx[1]]
    w, v = np.linalg.eigh(err_mtrx)
    x1, x2 = get_prob_contour(np.array([theta[idx[0]], theta[idx[1]]]), w, v)
    if ax == None:
        fig = plt.figure(figsize=(4, 3))
        ax = fig.add_subplot(111)
    ax.plot(x1[0, :], x2[0, :], label=label, **ax_kwargs)
    ax.plot(x1[1, :], x2[1, :], **ax_kwargs)
    return ax

def myDecimateFunc(xx0, fs0, fs, detrend=True):
    yy = xx0.copy()
    if detrend:
        # so far just include a linear detrend
        tt0=np.arange(0., len(xx0)/fs0, 1./fs0)
        poly_coef = np.polyfit(tt0, xx0, 1)
        yy -= (poly_coef[0]*tt0+poly_coef[1])
    
    down_factor = int(fs0/fs)
    fir_aa = sig.firwin(20*down_factor+2, 0.8/down_factor, \
        window='blackmanharris')
    yy=sig.decimate(yy, down_factor, \
        ftype=sig.dlti(fir_aa[1:-1], 1), zero_phase=True)
    
    if detrend:
        # add back the linear trend
        tt = np.arange(0., len(yy)/fs, 1./fs)
        yy += (poly_coef[0]*tt + poly_coef[1])
    return yy


def rms_from_psd(psd, freq):
    nPt = len(psd)
    rms = np.zeros(nPt)
    
    for i in range(nPt):
        rms[i] = np.trapz(psd[i:], freq[i:])
        
    rms = np.sqrt(rms)
    return rms